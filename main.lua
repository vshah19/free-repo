counter = 0
function main(Data)
   -- Sample data 1 has MSH 9 of ADT^A01^ADTA01A04, which fails the ADTA01 matching rule
   -- Sample data 2 has MSH 9 of ADT^A01, which passes the ADTA01 matching rule
   -- local H = hl7.parse{vmd='testvmd.vmd', data=Data}
   local H = hl7.parse{vmd='test1.vmd', data=Data}
   counter = counter + 1
   trace(counter)


end
